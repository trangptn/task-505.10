//Import thu vien expressJs 
const express =require('express');

//khoi tao app nodeJs bang express
const app= express();

//Khai bao ung dung se chay tren cong 8000
const port=8000;

//Khai bao api tra ve chuoi
app.get("/",(request,reponse)=>{
    //Viet code
    var today=new Date();

    var resultString=`Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()} `;

    reponse.json({
        result:resultString
    })
})

//Chay ung dung
app.listen(port,()=>{
    console.log("Ứng dụng chạy trên cổng: "+ port);
})



